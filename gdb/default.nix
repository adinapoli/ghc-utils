# Usage:  nix build -f ./. env
#
# Start `result/bin/gdb` and run `source result/gdbinit` once a Haskell executable is loaded.

let
  rev = "8a6e7487fb49a21edfa7abe9b6a054d71187698d";
  baseNixpkgs =
    builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
    sha256 = "1cm57h8afry4cfng2jr3axyi945a64llx9kzfjjy5gniywm31mgn";
  };
  defaultNixpkgs = import baseNixpkgs {};

in { nixpkgs ? defaultNixpkgs }:
with nixpkgs; rec {
  ghc-gdb = python3Packages.buildPythonPackage {
    name = "ghc-gdb";
    src = ./.;
    preferLocalBuild = true;
    checkInputs = [ mypy ];
    checkPhase =
      ''
        mypy --ignore-missing-imports .
      '';
  };

  run-ghc-gdb = writeScriptBin "ghc-gdb" ''
    #!${pkgs.bash}/bin/bash
    set -eu -o pipefail

    ${gdb}/bin/gdb -x ${gdbinit}/gdbinit "$@"
  '';

  run-ghc-rr = writeScriptBin "ghc-rr" ''
    #!${pkgs.bash}/bin/bash
    set -eu -o pipefail

    args="$@"
    if [[ "$1" == "replay" ]]; then
      args="$args --debugger ${gdb}/bin/gdb -x ${gdbinit}/gdbinit"
    fi
    ${rr}/bin/rr $args
  '';

  libipt = stdenv.mkDerivation {
    name = "libipt";
    nativeBuildInputs = [ cmake ];
    src = fetchFromGitHub {
      owner = "01org";
      repo = "processor-trace";
      rev = "f7a4ee88b32a0c2d148dd08268b94c4076da43d9";
      sha256 = "18wc5gk6vby3wr0rx9dgg841zb8yhsw1dbpvs6b1xs8c7j4q08mr";
    };
    #sourceRoot = "source/libipt";
  };

  gdb-walkers = nixpkgs.fetchFromGitHub {
    owner = "hardenedapple";
    repo = "gdb-walkers";
    rev = "01b346ba281b0c81f4ee0ba488ce57a7584f3877";
    sha256 = "1y3lbar2jn7hjcd2zixcw8ydgmg51apcsjax1nraj2wa5qj5kk52";
  };

  gdb = (nixpkgs.gdb.override {
    python3 = python3;
  }).overrideAttrs (oldAttrs: {
    buildInputs = oldAttrs.buildInputs ++ [ libipt ];
  });

  rr = nixpkgs.rr.overrideAttrs (oldAttrs: {
    src = fetchFromGitHub {
      owner = "mozilla";
      repo = "rr";
      rev = "42bf5814f990fca61c743013041d02a84d383926";
      sha256 = "0pqlcwy9smdjcmfghqlm8zp5p8rnsxa6xl2pr3a8x7bzgs9471ri";
    };
  });

  pythonEnv = python3.withPackages (_: [ ghc-gdb ]);

  env = symlinkJoin {
    name = "gdb-with-ghc-gdb";
    paths = [
      gdb pythonEnv gdbinit rr dot2svg
      run-ghc-gdb run-ghc-rr
    ];
  };

  # useful to render `ghc closure-deps` output
  dot2svg = writeScriptBin "dot2svg" ''
    #!${pkgs.bash}/bin/bash
    set -eu -o pipefail

    if [[ $# == 0 ]]; then
      echo "Usage: $0 [dot file]"
      exit 1
    fi
    ${graphviz}/bin/dot -T svg -o $1.svg $1
  '';

  gdbinit = writeTextFile {
    name = "gdbinit";
    destination = "/gdbinit";
    text = let
      ghc-gdb-init = ''
        python sys.path = ["${pythonEnv}/lib/python${python3.pythonVersion}/site-packages"] + sys.path
        python
        if 'ghc_gdb' in globals():
            import importlib
            importlib.reload(ghc_gdb)
        else:
            try:
                import ghc_gdb
            except Exception as e:
                import textwrap
                print('Failed to load ghc_gdb:')
                print('  ', e)
                print("")
                print(textwrap.dedent("""
                  If the failure is due to a missing symbol or type try
                  running `import ghc_gdb` after running the inferior.
                  This will load debug information that is lazily
                  loaded.
                """))
        end

        echo The `ghc` command is now available.\n
      '';

      gdb-walkers-init = ''
        python sys.path = ["${gdb-walkers}"] + sys.path
        source ${gdb-walkers}/basic_config.py
        source ${gdb-walkers}/commands.py
        source ${gdb-walkers}/functions.py
      '';
    in lib.concatStringsSep "\n" [ ghc-gdb-init gdb-walkers-init ];
  };
}

