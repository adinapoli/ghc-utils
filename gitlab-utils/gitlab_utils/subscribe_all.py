#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import gitlab

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project', '-p', type=str, required=True, help='Project name')
    parser.add_argument('--user', '-u', type=str, required=True, help='sudo user')
    parser.add_argument('--issues', '-i', action='store_true', help='issues')
    parser.add_argument('--merge-requests', '-m', action='store_true', help='merge requests')
    parser.add_argument('--dry-run', '-n', action='store_true', help="Don't change anything")
    args = parser.parse_args()
    
    proj = gl.projects.get(args.project)

    for x in proj.issues.list():
        print(x.iid)
        x.subscribe(sudo=args.user)

    for x in proj.merge_requests.list():
        print(x.iid)
        x.subscribe(sudo=args.user)

