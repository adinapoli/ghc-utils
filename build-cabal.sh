#!/usr/bin/env bash

set -e -x
out=_cabal_out
dir=libraries/Cabal/Cabal
if [[ -z "$GHC" ]]; then
  GHC="_build/stage1/bin/ghc"
fi
time $GHC -hidir $out -odir $out -i$dir -i$dir/src $dir/Setup.hs +RTS -s -RTS $@
rm -R $out
