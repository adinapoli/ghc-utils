#!/usr/bin/env bash

set -e -x

if [[ -z "$BUILD_FLAVOUR" ]]; then
  BUILD_FLAVOUR="validate"
fi

if [[ -z "$IMAGE" ]]; then
  IMAGE="$1"
  shift
fi

COMMIT="$2"
if [[ -z "$COMMIT" ]]; then
  COMMIT="master"
fi

#C=$(docker container create --name="ghc-docker" $IMAGE)

cidfile=$(mktemp)
rm $cidfile
docker run --cidfile=$cidfile -i $IMAGE bash <<EOF
set -e
git clone https://gitlab.haskell.org/bgamari/ghc-utils
git clone https://gitlab.haskell.org/ghc/ghc
git -C ghc checkout $COMMIT
git -C ghc submodule update --init
cd ghc
.gitlab/ci.sh setup
.gitlab/ci.sh configure
EOF

image=$(docker commit \
  --change "WORKDIR /home/ghc/ghc" \
  --change "ENV BIN_DIST_NAME tmp"
  --change "ENV BUILD_FLAVOUR $BUILD_FLAVOUR"
  $(cat $cidfile))
echo $image
rm -f $cidfile

docker run -it $image bash -i

